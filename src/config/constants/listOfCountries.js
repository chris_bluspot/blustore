import {
  countries,
  callingCountries,
} from 'country-data';

const listOfCountries = [
    {
      "code" : callingCountries.CO.countryCallingCodes,
      "flag": countries.CO.emoji,
      "short_name": 'CO'
    },
    {
      "code" : callingCountries.CL.countryCallingCodes,
      "flag": countries.CL.emoji,
      "short_name": 'CL'
    },
    {
      "code" : callingCountries.PA.countryCallingCodes,
      "flag": countries.PA.emoji,
      "short_name": 'PA'
    },
    {
      "code" : callingCountries.PE.countryCallingCodes,
      "flag": countries.PE.emoji,
      "short_name": 'PE'
    },
  ];

  export default listOfCountries;