import {
    PixelRatio
} from 'react-native';

const ratio = PixelRatio.get();

export {ratio};