import {
    Dimensions
} from 'react-native';

const height = Dimensions.get('screen').height;

const width = Dimensions.get('screen').width;


module.exports = {
    height,
    width
}