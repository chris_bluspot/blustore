const fonts = {
    'light': require('../../../assets/fonts/Oswald-Light.ttf'),
    'demiBold' : require('../../../assets/fonts/Oswald-DemiBold.ttf'),
    'bold' : require('../../../assets/fonts/Oswald-Bold.ttf'),
    'regular' : require('../../../assets/fonts/Oswald-Regular.ttf'),
    'medium': require('../../../assets/fonts/Oswald-Medium.ttf'),
}

export { fonts }