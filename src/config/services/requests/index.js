module.exports = {
    API: 'http://67.205.160.212:3000',
    CODEAPI: 'https://us-central1-bluspot-test.cloudfunctions.net',
    // API:  'https://api.bluspot.us', 
    // CODEAPI:  'https://us-central1-auth-480f8.cloudfunctions.net', 
    CREATEUSER: '/createUser',
    REQUESTONETIMEPASSWORD: '/requestOneTimePassword',
    ISAPIOK: '/isApiOk',
    CHECKPHASE: '/checkPhase',
    FINDSPOT: '/findSpot',
    VERIFYONETIMEPASSWORD: '/verifyOneTimePassword',
    UPDATEUSER: '/updateUser',
    CREATERESERVE: '/createReserve',
    CANCELRESERVE: '/cancelReserve',
    CONFIRMRESERVE: '/confirmReserve',
    FINDUSER: '/findUser',
    PAYSPOT: '/paySpot',
    SAVEFEEDBACK: '/saveFeedBack',
    GETMAXTIMERESERVE: '/getMaxTimeReserve',
    DELETECREDITCARD: '/deleteCreditCard',
    REGISTERPAYMENT: '/registerPayment',
    CREATEVEHICLE: '/createVehicle',
    EDITVEHICLE: '/editVehicle',
    DELETEVEHICLE: '/deleteVehicle',
    REQUESTFORCALL: '/requestForCall',
    MONTHLYRENT: '/monthlyRent',
    CANCELMONTHLYRENT: '/cancelMonthlyRent',
    SAVEWANTEDSPOTLOCATION: '/saveWantedSpotLocation',
    FINDPAYMENTHISTORY: '/findPaymentHistory',
    REPORTBUG: '/reportBug',
    GETNOTIFICATIONSFORUSER: '/getNotificationsForUser',
    VISUALIZEDBYUSER: '/visualizedByUser',
    GETFIRSTPENDINGMODAL: '/getFirstPendingModal',
    CERTIFYMYIDENTITY: '/certifyMyIdentity',
    CLAIMCOUPON: '/claimCoupon',
    CHARGEBLUER: '/chargeBluer',
    FINDCENTRALITIES: '/findCentralities',
    FINDSPOTSNEARLOCATION: '/findSpotsNearLocation',
    GETMYRENTS: '/getMyRents',
    FINDSPOTBYID: '/findSpotByID',
    GETIPDATA: '/getIPdata',
    INSERTANONIMOUSUSER: '/insertAnonimousUser',
    FINDANONYMOUSUSER:'/findAnonymousUser',
    ENCRYPTQRCOM: '/encryptQRCom',
    FINDRECIP: '/findRecip',
    RECORDPRODUCTIONLOG: '/recordProductionLog',
    REQUESTFORCODEWHATSAPP: '/requestForCodeWhatsapp',
    RECORDLOGINDATA: '/recordLoginData',
    FINDANALYTICS: '/findAnalytics',
};