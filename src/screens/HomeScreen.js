import React, { useState } from 'react';
import { View, Text, StyleSheet, ScrollView } from 'react-native';
import Header from '../components/Header';
import StatusBar from '../components/StatusBar';
import useResults from '../hooks/useResults';
import StoresList from '../components/StoresList';
import SearchBar from '../components/SearchBar';
import { height, width } from '../config/constants/screenDimensions';

const HomeScreen = () => {
    const [term, setTerm] = useState('');
    const [searchApi, results, errorMessage] = useResults();

    const filterResultsByPrice = (price) => {
        // price === '$' || '$$' || '$$$'
        return results.filter(result => {
            return result.price === price;
        });
    };

    return (
        <View style={styles.container}>
            <StatusBar />
            <Header
                title={'Tiendas'}
            />
            <SearchBar
                term={term}
                onTermChange={setTerm}
                onTermSubmit={() => searchApi(term)}
            />
            {errorMessage ? <Text>{errorMessage}</Text> : null}
            <StoresList
                title="Tiendas"
                results={filterResultsByPrice('$')}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    }
});

export default HomeScreen;