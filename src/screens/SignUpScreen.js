import React, { useState, useRef } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    Platform,
    TextInput,
    TouchableOpacity,
    Keyboard,
    Vibration,
} from 'react-native';
import { width, height } from '../config/constants/screenDimensions';
import colors from '../../assets/colors';
import normalize from '../config/services/normalizeFontSize';
import header from '../../assets/images/phoneNumberHeader.png';
import ModalDropdown from 'react-native-modal-dropdown';
import listOfCountries from '../config/constants/listOfCountries';
import {
    countries,
    callingCountries,
} from 'country-data';
import BluButton from '../components/BluButton';
import { NavigationActions } from 'react-navigation';
import validate from 'validate.js';
import TextError from '../components/TextError';
import {
    CODEAPI,
    CREATEUSER,
    //INSERTANONIMOUSUSER,
    REQUESTONETIMEPASSWORD,
    //API
} from '../config/services/requests';
import axios, { CancelToken } from 'axios';
import BluModal from '../components/BluModal';

const _renderCountryCodeRow = (rowData) => {
    const { code, flag } = rowData;
    return (
        <View style={styles.listSection}>
            <Text style={styles.flag}>{`${flag}`} </Text>
            <Text style={[styles.textBar, { textAlign: 'center' }]}> {`${code}`}</Text>
        </View>
    );
}

const _renderButtonText2 = (rowData) => {
    const { code, flag } = rowData;
    return `${flag} ${code}`;
};

const SignUpScreen = (props) => {
    const [phone, setPhone] = useState('');
    const [countriesList, setCountriesList] = useState(listOfCountries);
    const [defaultValue, setDefaultValue] = useState(countries.CO.emoji + ' ' + callingCountries.CO.countryCallingCodes);
    const [countryCode, setCountryCode] = useState(callingCountries.CO.countryCallingCodes);
    const [countryFlag, setCountryFlag] = useState(countries.CO.emoji);
    const [codeError, setCodeError] = useState('');
    const [error, setError] = useState('');
    const [loading, setLoading] = useState(false);
    const [phoneStore, setPhoneStore] = useState('');

    const success = useRef('success');
    const troubles = useRef('troubles');

    const handleSubmit = async () => {
        console.log('phone: ', phone);
        console.log('code: ', countryCode[0]);
        Keyboard.dismiss();
        setLoading(true);
        setError('');
        let max = 0;
        //Sets the max longitude the phone numbe rhas according to the country
        if (countryCode[0] === '+57') max = 10
        else if (countryCode[0] === '+56') max = 9
        else if (countryCode[0] === '+507') max = 8
        console.log('max', max);
        if (phone.length !== 0) {
            //Config for validate.js
            var constraints = {
                phone: {
                    presence: true,
                    numericality: {
                        onlyInteger: true,
                    },
                    length: {
                        minimum: max,
                        maximum: max,
                    },
                }
            };
            //Validate user input
            const pe = validate({ phone: phone }, constraints);
            if (!pe && countryCode != '') {

                const phoneNumber = countryCode + phone; //Country code
                setPhoneStore(phoneNumber)
                const code = countryCode[0] //Cellphone number
                let to = null
                try {
                    let source = CancelToken.source();
                    to = setTimeout(() => {
                        source.cancel('timeout');
                    }, 20000); //Sets time out with cancel token
                    await axios.post(`${CODEAPI}${CREATEUSER}`, { phone: phoneNumber }, { cancelToken: source.token });
                    clearTimeout(to) //clears the timeout
                    to = setTimeout(() => {
                        source.cancel('timeout');
                    }, 20000); //Sets time out with cancel token
                    await axios.post(`${CODEAPI}${REQUESTONETIMEPASSWORD}`, { phone: phoneNumber }, { cancelToken: source.token }); //API call to create user in firebase
                    clearTimeout(to) //clears the timeout
                    setLoading(false);
                    setPhone('');
                    success.current.open(); //Open success modal
                    //     publicIP()
                    //         .then(async ip => {
                    //             let to = null
                    //             try {
                    //                 let source = CancelToken.source();
                    //                 to = setTimeout(() => {
                    //                     source.cancel('timeout');
                    //                 }, 40000);
                    //                 clearTimeout(to)
                    //                 let data = {
                    //                     "phone": phone,
                    //                     "creationDate": new Date(),
                    //                     "ip": ip,
                    //                     "so": Platform.OS,
                    //                     "app": "bluer",
                    //                     "location": !_.isEqual(INIT, this.props.location) ? {
                    //                         "type": "Point",
                    //                         "coordinates": [
                    //                             this.props.location.coords.longitude,
                    //                             this.props.location.coords.longitude
                    //                         ]
                    //                     } : null
                    //                 }
                    //                 to = setTimeout(() => {
                    //                     source.cancel('timeout');
                    //                 }, 40000);
                    //                 await axios.post(`${API}${INSERTANONIMOUSUSER}`, data, { cancelToken: source.token })
                    //                 clearTimeout(to)
                    //                 this.setState({ ip })
                    //             } catch (err) {
                    //                 clearTimeout(to)
                    //                 //console.log(err)
                    //             }

                    //         })
                    //         .catch(error => {
                    //             //console.log(error);
                    //         });
                    // }
                    // //this.props.setUserPhone(phone) // save in the redux the phone
                    // //this.setState({ phone: '', loading: false }) //Sets the state to not loading.
                } catch (err) {
                    clearTimeout(to) //clears the timeout
                    //this.refs.success.close()
                    console.log(err)
                    if (err.response && err.response.status === 422) {   //If there was a request error
                        setError('Algo salió mal, revisa el número que ingresaste o vuelve a intentarlo más tarde.');
                        setLoading(false);
                    } else {
                        Vibration.vibrate(1000 * 0.3);
                        troubles.current.open() //Opens the troubles modal
                        setLoading(false);
                    }
                }
            }
            else if (countryCode === '') { //If code not specified
                setError("Por favor selecciona el código de tu país");
                setCodeError('3');
                setLoading(false);
            } else { //If cellphone number is in valid
                setPhone('');
                setError("Por favor ingresa un número válido. Recuerda que si ya te aparece el código de tu país no debes ingresarlo!");
                setCodeError('2');
                setLoading(false);
            }
        }
        else { //If input is empty
            setError('Por favor ingresa un número');
            setCodeError('1');
            setLoading(false);
        }
    };

    return (
        <View style={styles.container}>
            <Image
                source={header}
                style={styles.imageHeader}
            />
            <Text style={styles.register}>Regístrate</Text>
            <Text style={styles.cellPhone}>Ingresa tu número de celular</Text>
            <View style={styles.barStyle}>
                <ModalDropdown
                    options={countriesList}
                    defaultValue={defaultValue}
                    style={styles.dropDown}
                    dropdownStyle={styles.dropDownList}
                    textStyle={styles.textBar}
                    renderRow={rowData => _renderCountryCodeRow(rowData)}
                    renderButtonText={(rowData) => _renderButtonText2(rowData)}
                    onSelect={(idx, value) => {
                        setCountryCode(value.code);
                        setCountryFlag(value.flag);
                    }}
                />
                <View style={styles.barSeparator} />
                <TextInput
                    value={phone}
                    underlineColorAndroid={'transparent'}
                    maxLength={12}
                    onChangeText={phone => setPhone(phone)}
                    placeholder={'Número de teléfono'}
                    keyboardType={"phone-pad"}
                    style={[styles.textBar, styles.containerStyle]}
                />
                <Image
                    source={require('../../assets/images/phoneIcon.png')}
                    style={styles.phoneIcon}
                />
            </View>
            {codeError != '' ? <TextError text={error} /> : null}
            <BluButton
                title={'Envíame un código'}
                onPress={() => handleSubmit()}
                isDark={false}
                hasShadow={true}
                container={{ zIndex: -1, position: 'relative' }}
                hasActivityIndicator={true}
                activityIndicatorStatus={loading}
            />
            <TouchableOpacity
                style={styles.mailLoginContainer}
                onPress={() => {
                    Keyboard.dismiss();
                    props.navigation.reset([NavigationActions.navigate({ routeName: 'Home' })], 0)
                }}
            >
                <Text style={styles.gotToRegister}>
                    En otro momento
                </Text>
            </TouchableOpacity>
            <BluModal
                ref={success}
                title={'¡Éxito!'}
                text={'Enviamos un código a tu móvil para que completes tu registro'}
                buttonTitle={'Aceptar'}
                onPress={() => {
                    success.current.close();
                    props.navigation.navigate('Code', {userPhoneNumber: phoneStore});
                }}
                type={'success'}
                backdrop
                backdropColor={colors.bluspotColor}
                hasActivityIndicator={true}
                activityIndicatorStatus={loading}
                titleFontSize={21}
            />
            <BluModal
                ref={troubles}
                title={'Ooops algo salió mal'}
                text={'Actualiza la app para poder seguir usando Blustore.'}
                buttonTitle={'Aceptar'}
                onPress={() => troubles.current.close()}
                type={'warning'}
                backdrop
                backdropColor={colors.bluspotColor}
            />
        </View>
    );
};

const {
    lightShadow,
    darkGray,
    gray,
    blueGray,
    pattensBlue
} = colors;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        backgroundColor: 'white',
    },
    imageHeader: {
        width: width * 0.2,
        height: width * 0.2,
        marginTop: '40%',
    },
    register: {
        marginTop: '2%',
        color: lightShadow,
        fontSize: Platform.isPad ? normalize(24) : normalize(22),
        fontFamily: 'bold',
    },
    cellPhone: {
        fontFamily: 'light',
        color: darkGray,
        fontSize: normalize(20),
        marginTop: '2%',
        alignItems: 'center',
    },
    barStyle: {
        flexDirection: 'row',
        backgroundColor: gray,
        width: width * 0.82,
        height: height > 800 ? height * 0.065 : height * 0.08,
        marginTop: height * 0.05,
        borderRadius: 7,
        alignItems: 'center',
        marginBottom: height * 0.05
    },
    textBar: {
        color: blueGray,
        fontSize: Platform.isPad ? normalize(17) : normalize(15),
        fontFamily: 'light',
    },
    containerStyle: {
        width: '60%',
        height: height * 0.06,
        borderBottomWidth: 0,
        marginLeft: '4%',
        justifyContent: 'center',
    },
    phoneIcon: {
        width: 30,
        height: 30,
        alignSelf: 'center',
        right: Platform.isPad ? 5 :
            (Platform.OS === 'ios' && height === 812 && width === 375 ? 32 : 25),
    },
    dropDown: {
        justifyContent: 'center',
        alignItems: 'center',
        width: width * 0.21,
        height: height * 0.07,
    },
    dropDownList: {
        marginTop: height * - 0.01,
        width: width * 0.21,
        height: height * 0.21,
    },
    listSection: {
        backgroundColor: gray,
        flexDirection: 'row',
        width: width * 0.21,
        height: height * 0.07,
        justifyContent: 'center',
        alignItems: 'center',
    },
    flag: {
        height: 30,
        width: 30,
        fontSize: Platform.isPad ? normalize(29) : normalize(27),
        textAlign: 'center',
    },
    barSeparator: {
        height: height * 0.03,
        width: 0,
        borderLeftWidth: 1,
        borderColor: pattensBlue,
        alignSelf: 'center'
    },
    mailLoginContainer: {
        marginTop: 20,
    },
    gotToRegister: {
        marginTop: height * 0.005,
        fontSize: Platform.isPad ? normalize(21) : normalize(19),
        fontFamily: 'light',
        color: lightShadow,
        textAlign: 'center',
        zIndex: -1,
    },
});

export default SignUpScreen;