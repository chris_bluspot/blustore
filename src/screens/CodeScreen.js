import React, { useState, useRef } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image, Platform,
    TextInput,
    TouchableOpacity,
    Vibration,
    AsyncStorage,
    Keyboard
} from 'react-native';
import DismissKeyboardView from '../components/DismissKeyboard';
import BackButton from '../components/BackButton';
import normalize from '../config/services/normalizeFontSize';
import { height, width } from '../config/constants/screenDimensions';
import colors from '../../assets/colors';
import StatusBar from '../components/StatusBar';
import TextError from '../components/TextError';
import BluButton from '../components/BluButton';
import BluActivityIndicator from '../components/BluActivityIndicator';
import BluModal from '../components/BluModal';
import {
    CODEAPI,
    VERIFYONETIMEPASSWORD
    //CREATEUSER,
    //INSERTANONIMOUSUSER,
    //REQUESTONETIMEPASSWORD,
    //API
} from '../config/services/requests';
import validate from 'validate.js';
import axios, { CancelToken } from 'axios';
import firebase from 'firebase';
import * as SecureStore from 'expo-secure-store';

const CodeScreen = (props) => {
    const [code, setCode] = useState('');
    const [codeError, setCodeError] = useState('');
    const [loading, setLoading] = useState(false);
    const [resendCodeLoading, setResendCodeLoading] = useState(false);

    const userTroubles = useRef('userTroubles');
    const troubles = useRef('troubles');
    const codeRef = useRef('code');
    const codeSent = useRef('codeSent');

    const handleSubmit = async () => {
        setLoading(true) // sets the state to loading. This means an api call is in the process and must await it. So the button related to this method will be disabled 
        if (code != '') { //Text input is not empty
            //Config for validate.js
            var constraints = {
                code: {
                    presence: true,
                    numericality: {
                        onlyInteger: true,
                    },
                    length: {
                        minimum: 4,
                        maximum: 4,
                    },
                }
            };
            //Validate user input
            const ce = validate({ code: code }, constraints);
            //If there wasn't any error
            if (!ce) {
                let to = null
                try {
                    let source = CancelToken.source();
                    to = setTimeout(() => {
                        source.cancel('timeout');
                    }, 20000); //Sets time out with cancel token
                    let { data } = await axios.post(`${CODEAPI}${VERIFYONETIMEPASSWORD}`,
                        { phone: props.navigation.state.params.userPhoneNumber, code: code },
                        { cancelToken: source.token }); //API call to firebase (CODEAPI) which handles the onetimepassword. In this api call the code is verified.
                    clearTimeout(to) //clears the timeout
                    console.log('data: ', data)
                    //await firebase.auth().signInWithCustomToken(data.token) //signs in the user to firebase if the code was correct
                    //await props.setToken(data.token) // stores tje firebase token in redux if the code was correct
                    if (Platform.OS === 'android' && Platform.Version < 23) {
                        await AsyncStorage.setItem('firebaseToken', data.token)
                    } else {
                        await SecureStore.setItemAsync('firebaseToken', data.token)
                    }
                    Keyboard.dismiss() //Hides keyboard
                    setCode('') //clears input
                    setLoading(false);
                    props.navigation.navigate('Home');
                } catch (err) {
                    console.log(err);
                    // console.log(err.response);
                    clearTimeout(to) //clears the timeout
                    setLoading(false) //Sets the state to not loading.
                    //console.log('Se presento un error en updateUser o checkPhase o signInWithCustomToken: ' + err)
                    if (err.response && err.response.data.error === "Code not valid") { //If the error was produced due to a bad code
                        setLoading(false);
                        setCode('');
                        setCodeError('');
                        codeRef.current.open() //Opens code modal
                    } else { //Any other error
                        setLoading(false);
                        setCode('');
                        setCodeError('');
                        Vibration.vibrate(1000 * 0.3);
                        troubles.current.open();
                    }
                }
            } else { //User ingressed something not valid like a letter
                setLoading(false);
                setCode('')
                setCodeError('El código ingresado no es válido. Compruébalo.')
            }
        } else { //user pressed the button with input empty
            setLoading(false);
            setCodeError('Por favor, ingresa el código que te enviamos')
        }
    }

    const resendCode = () => {
        console.log('resend');
    }

    const goBack = () => {
        console.log('go back');
    };

    console.log('userPhone: ', props.navigation.state.params.userPhoneNumber);
    return (
        <DismissKeyboardView>
            <StatusBar />
            <View style={styles.container}>
                <BackButton
                    type={'back'}
                    onPress={() => props.navigation.navigate('SignUp')}
                    container={{ alignSelf: 'flex-start' }}
                />
                <Image
                    source={require('../../assets/images/phoneNumberHeader.png')}
                    style={styles.imageHeader}
                />
                <Text style={styles.register}>{' Ingresa tu código '}</Text>
                <Text style={[styles.politics, { marginTop: '2%' }]}>{' Código enviado al '}
                    <Text style={[styles.warningLink, { textDecorationLine: 'none' }]}> {props.navigation.state.params.userPhoneNumber} </Text>
                </Text>
                <View style={styles.barStyle}>
                    <TextInput
                        value={code}
                        placeholder={'----'}
                        keyboardType={'numeric'}
                        maxLength={4}
                        onChangeText={code2 => setCode(code2)}
                        underlineColorAndroid={'transparent'}
                        style={[styles.textBar, styles.codeBar]}
                    />
                    <Image
                        source={require('../../assets/images/pen.png')}
                        style={styles.pen}
                    />
                </View>
                {codeError != '' ?
                    <TextError text={codeError} />
                    : null
                }
                <BluButton
                    title={'Siguiente'}
                    onPress={() => handleSubmit()}
                    isDark={false}
                    hasShadow={true}
                    hasActivityIndicator={true}
                    activityIndicatorStatus={loading}
                />

                {/* <View style={styles.politicsContainer}>
                    <View>
                        <Text style={styles.politics}>
                            <Text style={styles.asterisk}>*</Text>
                            {t('code.politics')}
                        </Text>
                    </View>
                    <TouchableOpacity
                        onPress={() => {
                            //this.props.goBack('code')
                            //this.props.navigation.navigate('terms')}
                            const terms = NavigationActions.navigate({
                                routeName: 'terms',
                                params: {
                                    backTo: 'code'
                                }
                            });
                            this.props.navigation.dispatch(terms);
                        }
                        }
                    >
                        <Text style={styles.warningLink}>{t('code.politics2')}</Text>
                    </TouchableOpacity>
                </View> */}
                {/* <View style={styles.secondaryPoliticsContainer}>
                    <View>
                        <Text style={styles.politics}>
                            {t('code.politics3')}
                        </Text>
                    </View>
                    <TouchableOpacity
                        onPress={() => {
                            //this.props.goBack('code')
                            //this.props.navigation.navigate('politics')
                            const politics = NavigationActions.navigate({
                                routeName: 'politics',
                                params: {
                                    backTo: 'code'
                                }
                            });
                            this.props.navigation.dispatch(politics);
                        }}
                    >
                        <Text style={[styles.warningLink, { marginTop: '3%', }]}>{t('code.politics4')}</Text>
                    </TouchableOpacity>
                </View> */}
                {!resendCodeLoading ?
                    <TouchableOpacity
                        style={{ backgroundColor: 'transparent' }}
                        onPress={() => resendCode()}
                    >
                        <Text style={styles.resendCode}>{'Reenviar código'}</Text>
                    </TouchableOpacity>
                    : <BluActivityIndicator color={colors.lightShadow} />
                }
                {/* <BluButton
                    title={'Enviar el código a mi WhatsApp'}
                    onPress={this.sendWhatsAppCode}
                    isDark={false}
                    hasShadow={true}
                    hasActivityIndicator={false}
                    container={styles.wazeButton}
                    textStyle={{ color: colors.lightShadow, }}
                    img={require('../../assets/images/whats-app-icon.png')}
                    imgStyle={{ height: 30, width: 30, }}
                    hasActivityIndicator={true}
                    activityIndicatorStatus={this.state.whatsAppCode}
                /> */}
                <BluModal
                    ref={userTroubles}
                    title={'Ooops algo salió mal'}
                    text={'Hubo un problema con tu sesión. Vuelve a intentarlo más tarde.'}
                    buttonTitle={'Aceptar'}
                    type={'warning'}
                    onPress={() => goBack()}
                    hasActivityIndicator={false}
                />
                <BluModal
                    ref={codeRef}
                    title={"Código no válido, renueva tu código"}
                    text={"Presiona Renovar para reenviar un nuevo código a tu celular."}
                    buttonTitle={"Renovar"}
                    type={'warning'}
                    onPress={() => resendCode()}
                    hasActivityIndicator={true}
                    activityIndicatorStatus={resendCodeLoading}
                />
                <BluModal
                    ref={troubles}
                    title={'Ooops algo salió mal'}
                    text={'Revisa tu conexión a internet Y vuelve a intentarlo más tarde'}
                    buttonTitle={'Aceptar'}
                    type={'warning'}
                    onPress={() => troubles.current.close()}
                    hasActivityIndicator={false}
                />
                {/* <BluModal
                    ref={'successCall'}
                    title={t('code.successCallModal.title')}
                    buttonTitle={t('common:modal.success.buttonTitle')}
                    onPress={() => this.refs.successCall.close()}
                    type={'success'}
                    hasActivityIndicator={false}
                    titleFontSize={21}
                /> */}
                <BluModal
                    ref={codeSent}
                    title={'¡Éxito!'}
                    text={'Reenviamos un código a tu móvil para que completes tu registro'}
                    buttonTitle={'Aceptar'}
                    onPress={() => codeSent.current.close()}
                    type={'success'}
                    hasActivityIndicator={false}
                    titleFontSize={21}
                />
            </View>
        </DismissKeyboardView>
    );
};

const {
    lightShadow,
    darkGray,
    gray,
    blueGray,
    pattensBlue
} = colors;

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        alignItems: 'center',
        height: height,
        width: width
    },
    imageHeader: {
        width: width * 0.2,
        height: width * 0.2,
        marginTop: '15%'
    },
    register: {
        marginTop: height * 0.003,
        color: lightShadow,
        fontSize: Platform.isPad ? normalize(24) : normalize(22),
        fontFamily: 'bold',
    },
    barStyle: {
        flexDirection: 'row',
        backgroundColor: gray,
        width: width * 0.82,
        height: height > 800 ? height * 0.065 : height * 0.08,
        marginTop: height * 0.05,
        marginBottom: height * 0.03,
        borderRadius: 7,
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    textBar: {
        color: blueGray,
        fontSize: Platform.isPad ? normalize(17) : normalize(15),
        fontFamily: 'light',
    },
    codeBar: {
        fontSize: normalize(20),
        fontFamily: 'bold',
        textAlign: 'center',
        justifyContent: 'center',
        width: width * 0.6,
        marginLeft: width * 0.1,
    },
    pen: {
        width: 32,
        height: 30,
        alignSelf: 'center',
        right: 5,
    },
    politicsContainer: {
        width: '100%',
        flexDirection: 'row',
        ...Platform.select({
            ios: {
                marginTop: height * 0.03,
            },
            android: {
                marginTop: height * 0.03,
            }
        }),
        justifyContent: 'center',
    },
    politics: {
        marginTop: height * 0.005,
        color: colors.blueGray,
        fontFamily: 'light',
        fontSize: Platform.isPad ? normalize(18) : normalize(16),
    },
    asterisk: {
        color: colors.lightShadow,
        fontSize: Platform.isPad ? normalize(21) : normalize(19),
        fontFamily: 'bold',
    },
    warningLink: {
        textAlign: 'center',
        fontSize: Platform.isPad ? normalize(18) : normalize(17),
        marginTop: height * 0.009,
        color: colors.lightShadow,
        textDecorationLine: 'underline',
        fontFamily: 'regular',
    },
    secondaryPoliticsContainer: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
    },
    resendCode: {
        textAlign: 'center',
        textDecorationLine: 'underline',
        fontSize: Platform.isPad ? normalize(19) : normalize(18),
        marginTop: height * 0.03,
        color: colors.blueGray,
        fontFamily: 'light',
    },
    wazeButton: {
        backgroundColor: 'white',
        borderWidth: 2,
        borderColor: colors.lightShadow,
        justifyContent: 'space-evenly',
        width: '60%',
        top: '7%'
    }
});

export default CodeScreen;