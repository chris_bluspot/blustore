import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, FlatList, Image, Platform } from 'react-native';
import yelp from '../api/yelp';
import StatusBar from '../components/StatusBar';
import Header from '../components/Header';
import normalize from '../config/services/normalizeFontSize';
import { height, width } from '../config/constants/screenDimensions';
import colors from '../../assets/colors';

const ResultsShowScreen = ({ navigation }) => {
  const [result, setResult] = useState(null);
  const id = navigation.getParam('id');

  const getResult = async (id) => {
    const response = await yelp.get(`/${id}`);
    setResult(response.data);
  };

  useEffect(() => {
    getResult(id);
  }, []);

  if (!result) {
    return null;
  }

  return (
    <>
      <StatusBar />
      <View style={styles.container}>
        <Header
          title={result.name}
          onPress={() => navigation.navigate('Home')}
          noShadow={true}
          buttonType={'back'}
          buttonImageStyle={{ height: 30, width: 30 }}
        />
        <Text style={styles.addressTitle}>Dirección:</Text>
        <Text style={styles.addressText}>{result.location.address1}, {result.location.city}</Text>
        <Text style={styles.addressTitle}>Teléfono:</Text>
        <Text style={styles.addressText}>{result.display_phone}</Text>
        <FlatList
          data={result.photos}
          keyExtractor={(photo) => photo}
          renderItem={({ item }) => {
            return <Image
              source={{ uri: item }}
              style={styles.image}
            />
          }}
        />
      </View>
    </>
  );
};

const {
  lightShadow,
  darkGray,
  gray,
  blueGray,
  pattensBlue
} = colors;

const styles = StyleSheet.create({
  container:{
    backgroundColor: 'white',
    flex:1
  },
  image: {
    height: 200,
    width: 300,
    marginTop: '5%',
    marginLeft: '5%',
    borderRadius: 15
  },
  addressTitle: {
    marginTop: height * 0.01,
    color: lightShadow,
    fontSize: Platform.isPad ? normalize(24) : normalize(22),
    fontFamily: 'bold',
    marginLeft: '5%'
  },
  addressText: {
    marginTop: height * 0.005,
    color: colors.blueGray,
    fontFamily: 'light',
    fontSize: Platform.isPad ? normalize(22) : normalize(20),
    marginLeft: '5%'
  }
});

export default ResultsShowScreen;
