import React, { Component } from 'react';
import {
    ActivityIndicator,
} from 'react-native';

const BluActivityIndicator = (props) => {
    const {
        color,
        style
    } = props;
    return (
        <ActivityIndicator size="large" color={color} style={style} />
    )
}

export default BluActivityIndicator