import React from 'react';
import { 
    Image,
    TouchableOpacity,
    View,
    StyleSheet
} from 'react-native';
import back from '../../assets/images/back.png';
import close from '../../assets/images/close.png';
import darkClose from '../../assets/images/dark-close.png';
import closeBlu from '../../assets/images/close-blu.png';
import { height, width } from '../config/constants/screenDimensions';

const BackButton = ({
    type,
    container,
    imageStyle,
    onPress,
    image,
    dark
}) => {
    const source = type === 'back' ? back :  type=== 'close' ? (dark ? darkClose : close) : (type=== 'close modal' ? closeBlu : image);
    return(
      <View>
          <TouchableOpacity
            onPress={onPress}
            style={[styles.container, container]}
            >
            <Image
                source = {source}
                style={[styles.button, imageStyle]}
            />
            </TouchableOpacity> 
      </View>  
    );
};

const styles = StyleSheet.create({
    container: {
        alignSelf: 'flex-start'
    },
    button: {
        height : width * 0.1, 
        width : width * 0.1
    }
});

export default BackButton;