import React from 'react';
import {
    Text,
    StyleSheet,
    Platform
} from 'react-native';
import normalize from '../config/services/normalizeFontSize';
import { height, width } from '../config/constants/screenDimensions';

const TextError = (props) => {
    return (
        <Text style={[styles.textError, props.style]}>{props.text}</Text> 
    )
}

const styles = StyleSheet.create({
    textError: {
        color:'red',
        fontSize: Platform.isPad ? normalize(22) : normalize(20),
        marginTop: height * 0.02,
        textAlign: 'center',
        width: '95%',
        fontFamily:'light'
    },
})


export default TextError;