import React, { Component } from 'react';
import { StyleSheet,View, StatusBar as Sb} from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';

const StatusBar = (props) => {
    return(
        <>
            <Sb  barStyle="dark-content" translucent={false} backgroundColor='#00AFD7'/>
            <View style={styles.statusBar}></View>
        </>
    );
};

const styles = StyleSheet.create({
    statusBar: {
        width: '100%',
        height: getStatusBarHeight(),
        //backgroundColor: '#00AFD7',
        backgroundColor: 'white'
    }
});

export default StatusBar;