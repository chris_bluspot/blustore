/**
 * BluModal componenet. This componenet is a modal created for Bluspot apps.
 * A modal is a little window that displays over everything.
 * 
 */
// Libraries imports
//React and react-native imports
import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    Animated,
    Platform,
    TouchableOpacity,
    Linking,
    StyleSheet,
} from 'react-native';
//Library that handles props types and requireness
import PropTypes from 'prop-types';
//react navigation imports. This functions handle the navigation through the app screens.
/**
 * Function that links Bluspot to published apps
 */
import AppLink from 'react-native-app-link';
import { WebView } from 'react-native-webview';
// Styling imports
import colors from '../../assets/colors';
//Custom Components imports
import BluButton from './BluButton';
import BluActivityIndicator from './BluActivityIndicator';
import DismissKeyboard from './DismissKeyboard';
import Header from './Header';
//Concfig imports
import normalize from '../config/services/normalizeFontSize';
import { height, width } from '../config/constants/screenDimensions';
//Image imports
import success from '../../assets/images/ready1.png';
import troubles from '../../assets/images/troubles.png';
import pnHeader from '../../assets/images/phoneNumberHeader.png';
//Class definition. Here resides all the class states, varaibles and functions including the rendering method
class BluModal extends Component {
    //Component's props default values or initializers
    static defaultProps = {
        type: 'success',
        short: true,
        hasButton: true,
        hasActivityIndicator: false,
        activityIndicatorStatus: false,
        onClose: () => { return; }
    };
    //Component constructor function
    constructor(props) {
        super(props);
        //BluModal states defition
        this.state = {
            isOpen: false, //Specifies if the modal is open or not
            opacity: 0, //Specifies the opacity level for the modal's background
            c: 0, //Aux variable to calculate position
            Y_LIMIT: 0, //Limit to animate in Y axis
        };
        //Initialize Animated value for the modal
        const position = new Animated.ValueXY({ x: 0, y: 0 });
        //Initialize modal's position
        this.position = position;
        this.videoPlayer = null
        /**
         * This section contains all the methods that requieres a binding in order to be used in external components such as SpotTicket
         */
        this.open = this.open.bind(this);
        this.close = this.close.bind(this);
        this.isOpen = this.isOpen.bind(this);
    }
    /**
     * Function that is called when the component renders for the first time. 
     */
    componentDidMount() {
        Animated.timing(this.position, {
            toValue: { x: 0, y: height },
            duration: 600,
        }).start();
    }
    //Function that gets the layout for the spot ticket animated component
    getCardStyle() {
        return this.position.getLayout();
    }

    onShouldStartLoadWithRequest = (navigator) => {
        if (navigator.url.indexOf('embed') !== -1
        ) {
            return true;
        } else {
            this.videoPlayer.stopLoading(); //Some reference to your WebView to make it stop loading that URL
            return false;
        }
    }

    /**
     * Function that handles the opening animation for the modal. It animates right from the bottom.
     */
    open = async () => {
        const c = this.props.type === 'custom' || !this.props.short ? 190 : 35;
        const Y_LIMIT = Platform.OS === 'ios' ? -(height / c) : height / 25;
        this.setState({ isOpen: true, opacity: 0.5, })
        Animated.spring(this.position, {
            toValue: { x: 0, y: this.state.Y_LIMIT },
            speed: 7,
        }).start();
    }
    /**
     * Function that handles 
     */
    openURLorApp = (config) => {
        //If type is app then it means the modal has to open a published app in appstore and playstore   
        if (config.type === 'app') {
            let link = config.link
            if (config.link === 'store') {
                if (Platform.OS === 'android') Linking.openURL('market://details?id=com.blugroup.bluspot')
                else Linking.openURL('https://apps.apple.com/app/bluspot-para-parquear/id1434002359?mt=8')
            } else AppLink.maybeOpenURL(link, { appName: config.appName, appStoreId: config.appStoreId, playStoreId: config.playStoreId }).then(() => {
                // do stuff
            }).catch((err) => {
                //console.log(err)
                this.refs.reserveTroubles.open()
            });
        } else { //Else it slhoud open a native app or web URL 
            Linking.openURL(config.link)
        }
    }
    /**
     * Function that handles the closing animation for the modal. 
     * It animates right from current position to the bottom.
     */
    close = () => {
        Animated.timing(this.position, {
            toValue: { x: 0, y: height },
            duration: 400,
        }).start(() => {
            this.setState({
                isOpen: false,
            })
            this.props.onClose();
        }
        );
        this.setState({
            opacity: 0,
        })
    }
    //Function that returns whether the modal is open or not
    isOpen = () => { return this.state.isOpen }
    //Function that renders one or two buttons. Depending on buttonGroup prop.
    renderButton() {
        const {
            type,
            buttonTitle,
            onPress,
            buttonGroup,
            secondButtonTitle,
            secondOnPress,
            hasActivityIndicator,
            activityIndicatorStatus,
            activityIndicatorStyle,
            isDark,
            functionType,
            headerOnPress
        } = this.props; //Props deconstruction
        if (buttonGroup) { //If a button group is needed
            //Checks if there's any loading API call related to this group. In affirmative case returns an activity indicator
            if (hasActivityIndicator && activityIndicatorStatus) return <BluActivityIndicator color={isDark ? colors.darkBlue : colors.bluspotColor} style={activityIndicatorStyle} />
            return (
                <View style={styles.buttonGroup}>
                    <BluButton
                        title={buttonTitle}
                        onPress={onPress}
                        isDark={type === 'success' ? false : true}
                        hasShadow={true}
                        hasActivityIndicator={false}
                    />
                    <BluButton
                        title={secondButtonTitle}
                        onPress={secondOnPress}
                        isDark={false}
                        hasShadow={true}
                        hasActivityIndicator={false}
                        container={{ backgroundColor: colors.gray }}
                        textStyle={{ color: type === 'success' ? colors.lightShadow : colors.darkBlue }}
                    />
                </View>
            )
        }
        return (
            <BluButton
                title={buttonTitle}
                onPress={functionType ? (functionType === 'close' ? headerOnPress : () => this.openURLorApp(functionType)) : onPress}
                isDark={type === 'success' ? false : true}
                hasShadow={true}
                hasActivityIndicator={hasActivityIndicator}
                activityIndicatorStatus={activityIndicatorStatus}
                activityIndicatorStyle={activityIndicatorStyle}
            />
        )
    }
    //Function that renders the multimedia conent for the multimedia modal
    renderImageMedia = (dimentions, mediaURL) => {
        return (
            <Image
                source={{ uri: mediaURL }}
                style={{ height: dimentions.height, width: dimentions.width, }}
                resizeMode={'contain'}
            />
        )
    }
    //Function that renders the component BluModal
    render() {
        const {
            opacity,
            isOpen,
        } = this.state; //State deconstruction
        const {
            title,
            text,
            textSecondary,
            type,
            short,
            modalStyle,
            containerStyle,
            buttonTextStyle,
            hasButton,
            textForReplace,
            imgType,
            hasCloseButton,
            headerOnPress,
            imgSource,
            imgStyle,
            titleFontSize,
            hasTextInput,
            textInputValue,
            onChangeText
        } = this.props; //Props deconstruction 
        let mainContainer = isOpen ? { top: 0 } : null; //Aux that modifies top styling property 
        let modal = short ? (type === 'success' ? styles.shortModalForSuccess : styles.shortModal)
            : styles.modal; //Aux that specifies the modal, styling depending in it's type
        let animatedViewStyle = short ? { height: '55%', width: '80%' } : null;
        let modalViewStyle = short ? { height: '100%', paddingTop: '10%', paddingBottom: '5%' } : null;
        if (type === 'custom') { // if type custom  returns the container and animated container. Then renders child props inside of it.
            return (
                <View
                    importantForAccessibility="yes"
                    accessibilityViewIsModal={true}
                    style={[
                        styles.container,
                        mainContainer,
                        containerStyle
                    ]}
                    pointerEvents={'box-none'}
                >
                    <View style={[styles.backdrop, { opacity: opacity }]} />
                    <Animated.View style={[this.getCardStyle(), modal, modalStyle, animatedViewStyle]}>
                        <DismissKeyboard>
                            <View style={[modal, modalStyle, modalViewStyle]}>

                                {hasCloseButton ?
                                    <Header
                                        withoutTitle={true}
                                        titleStyle={{ right: '80%', bottom: '100%' }}
                                        onPress={headerOnPress}
                                        buttonType={'close modal'}
                                        buttonImageStyle={{ height: 30, width: 30 }}
                                        buttonStyle={{ height: 30, width: 30, zIndex: 5000 }}
                                        dark={this.props.type === 'warning'}
                                        container={{
                                            ...Platform.select({
                                                ios: {
                                                    shadowOffset: {
                                                        width: 0,
                                                        height: 0
                                                    },
                                                },
                                                android: {
                                                    elevation: 0,
                                                }
                                            }),
                                            backgroundColor: 'transparent'
                                        }}
                                        customStyle={styles.noShadowHeader}
                                    />
                                    : null
                                }
                                {this.props.children}

                            </View>
                        </DismissKeyboard>
                    </Animated.View>
                </View>
            )
        } else if (type === 'multimedia') { //if type multimedia. Renders the multimedia inside the container and animated container
            const {
                media,
                dimentions,
                openAtPress,
                urlToOpen,
                mediaURL
            } = this.props //Props deconstruction
            if (media === 'img') return (
                <View
                    importantForAccessibility="yes"
                    accessibilityViewIsModal={true}
                    style={[styles.container, mainContainer, containerStyle]}
                    pointerEvents={'box-none'}
                >
                    <View style={[styles.backdrop, { opacity: opacity }]} />
                    <Animated.View style={[this.getCardStyle(), modal, dimentions, { backgroundColor: 'transparent' }]}>
                        <DismissKeyboard>
                            <Header
                                title={''}
                                onPress={headerOnPress}
                                buttonType={'back'}
                                container={[styles.header, { backgroundColor: 'transparent', zIndex: 3000, position: 'absolute' }]}
                                buttonImageStyle={{ marginTop: 50, height: 30, width: 30 }}
                                dark={this.props.type === 'warning'}
                                customStyle={styles.noShadowHeader}
                            />
                            {openAtPress ?
                                <TouchableOpacity
                                    onPress={() => {
                                        Linking.openURL(urlToOpen)
                                        headerOnPress()
                                    }}
                                >
                                    {this.renderImageMedia(dimentions, mediaURL)}
                                </TouchableOpacity>
                                : this.renderImageMedia(dimentions, mediaURL)
                            }
                        </DismissKeyboard>
                    </Animated.View>
                </View>
            )
            else {
                return (
                    <View
                        importantForAccessibility="yes"
                        accessibilityViewIsModal={true}
                        style={[styles.container, mainContainer, containerStyle]}
                        pointerEvents={'box-none'}
                    >
                        <View style={[styles.backdrop, { opacity: opacity }]} />
                        <Animated.View style={[this.getCardStyle(), dimentions, modal, { backgroundColor: 'transparent', elevation: 0 }]}>
                            <Header
                                title={''}
                                onPress={headerOnPress}
                                buttonType={'back'}
                                container={[styles.header, { backgroundColor: 'transparent', zIndex: 3000, position: 'absolute' }]}
                                buttonImageStyle={{ marginTop: 10, height: 30, width: 30 }}
                                dark={this.props.type === 'warning'}
                                customStyle={styles.noShadowHeader}
                            />
                            <View style={[dimentions, { borderRadius: 10 }]}>
                                <WebView
                                    source={{ uri: mediaURL }}
                                    style={[dimentions, { borderRadius: 10, }]}
                                    scalesPageToFit={true}
                                    javaScriptEnabled={true}
                                    domStorageEnabled={true}
                                />
                            </View>

                        </Animated.View>
                    </View>
                )
            }
        } else { //Common BluModal
            let imageSource = imgType === 'pnh' ? pnHeader : (type === 'success' ? success : troubles); //BluModal image
            let imagesStyles = type === 'success' ? styles.imageSuccess : styles.imageTroubles; //Styling depending on type
            let titleStyle = type === 'success' ? [styles.title, styles.successTitle] : styles.title; //Styling depending on type
            let mainTextStyle = type === 'success' ? [styles.text, styles.successText] : styles.text; //Styling depending on type
            mainTextStyle = textSecondary ? [mainTextStyle, { fontSize: 18, fontFamily: 'bold', fontWeight: 'bold' }] : mainTextStyle;
            let secondaryTextStyle = [styles.text, { fontSize: 14, marginTop: 20, width: '92%' }];
            imagesStyles = textSecondary ? imagesStyles : [imagesStyles, { height: 90, width: 90 }];
            imagesStyles = type === 'success' ? imagesStyles : styles.imageTroubles;
            return (

                <View
                    importantForAccessibility="yes"
                    accessibilityViewIsModal={true}
                    style={[styles.container, mainContainer, containerStyle,]}
                    pointerEvents={'box-none'}
                >
                    <View style={[styles.backdrop, { opacity: opacity }]} />
                    <Animated.View style={[this.getCardStyle(), modal, modalStyle, animatedViewStyle]}>
                        <DismissKeyboard>
                            <View style={[modal, modalViewStyle]} >

                                {hasCloseButton ?
                                    <Header
                                        withoutTitle={true}
                                        titleStyle={{ right: '80%', bottom: '100%' }}
                                        onPress={headerOnPress}
                                        buttonType={'close modal'}
                                        container={styles.header}
                                        buttonImageStyle={{ marginTop: 0, height: 30, width: 30 }}
                                        buttonStyle={{ height: 30, width: 30, zIndex: 5000 }}
                                        dark={this.props.type === 'warning'}
                                        customStyle={styles.noShadowHeader}
                                    />
                                    : null
                                }
                                {textSecondary ?
                                    <>
                                        <View style={styles.textContainer}>
                                            <Image
                                                source={imgSource || imageSource}
                                                style={imgStyle || imagesStyles}
                                            />
                                            <Text style={[titleStyle, { fontSize: titleFontSize }]}>{title}</Text>
                                            <Text style={mainTextStyle}> {text} </Text>
                                        </View>
                                        <Text style={secondaryTextStyle}> {textSecondary} </Text>
                                    </>
                                    :
                                    <>
                                        <Image
                                            source={imgSource || imageSource}
                                            style={imgStyle || imagesStyles}
                                        />
                                        <View style={styles.textContainer}>
                                            <Text style={[titleStyle, { fontSize: titleFontSize }]}>{title}</Text>
                                            <Text style={mainTextStyle}> {text} </Text>
                                        </View>
                                    </>
                                }
                                {hasTextInput ?
                                    <TextInput
                                        placeholder={'7'}
                                        placeholderTextColor={'#c0cbcd'}
                                        value={textInputValue}
                                        underlineColorAndroid={'transparent'}
                                        onChangeText={onChangeText}
                                        style={styles.textInput}
                                        keyboardType={'numeric'}
                                    />
                                    : null
                                }
                                {this.props.children}
                                {hasButton === false ?
                                    <Text style={styles.replacement} >{textForReplace}</Text>
                                    :
                                    this.renderButton()
                                }

                            </View>
                        </DismissKeyboard>
                    </Animated.View>
                </View>

            )
        }
    }
}
//BluModal prop types definition
BluModal.propTypes = {
    buttonTitle: PropTypes.string,
    title: PropTypes.string,
    text: PropTypes.string,
    type: PropTypes.string.isRequired,
    short: PropTypes.bool,
    modalStyle: PropTypes.object,
    containerStyle: PropTypes.object,
    mainTextStyle: PropTypes.object,
    titleStyle: PropTypes.object,
    textStyle: PropTypes.object,
    buttonTextStyle: PropTypes.object,
    hasButton: PropTypes.bool,
    hasActivityIndicator: PropTypes.bool,
    onPress: PropTypes.func,
    textForReplace: PropTypes.string,
    activityIndicatorStatus: PropTypes.bool,
    activityIndicatorStyle: PropTypes.object,
    buttonGroup: PropTypes.bool,
    secondButtonTitle: PropTypes.string,
    secondOnPress: PropTypes.func,
    imgType: PropTypes.string,
    hasCloseButton: PropTypes.bool,
    headerOnPress: PropTypes.func,
};
//Export the component

const {
    blue,
    darkBlue,
    darkGray
} = colors;

const styles = StyleSheet.create({
    container: {
        height: height,
        width: width,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 2000,
        elevation: 20,
        top: height + 150,
    },
    backdrop: {
        height: height,
        width: width,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.bluspotColor,
        position: 'absolute',
        zIndex: 2000,
    },
    modal: {
        height: height,
        width: width,
        backgroundColor: 'white',
        zIndex: 2000,
        alignItems: 'center',
        elevation: 20,
    },
    shortModal: {
        height: height * 0.6,
        width: width * 0.8,
        borderRadius: 10,
        alignItems: 'center',
        backgroundColor: 'white',
        justifyContent: 'space-around',
        zIndex: 2000,
        elevation: 12,
    },
    shortModalForSuccess: {
        height: height * 0.46,
        width: width * 0.75,
        backgroundColor: 'white',
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'space-around',
        zIndex: 2000,
    },
    imageTroubles: {
        width: width * 0.5,
        height: height * 0.18,
    },
    imageSuccess: {
        height: 60,
        width: 60
    },
    title: {
        fontFamily: 'bold',
        fontSize: normalize(19),
        textAlign: 'center',
        color: colors.darkBlue,
    },
    successTitle: {
        color: colors.blue,
        fontSize: Platform.isPad ? normalize(30) : normalize(28),
    },
    text: {
        color: 'black',
        textAlign: 'center',
        fontSize: normalize(16),
        fontFamily: 'light',
    },
    successText: {
        color: darkGray,
        fontSize: Platform.isPad ? normalize(22) : normalize(20),
    },
    textContainer: {
        width: '95%',
        flexDirection: 'column',
        alignItems: 'center',
    },
    replacement: {
        color: colors.lightGray,
        fontFamily: 'demiBold',
        width: '90%',
        textAlign: 'center',
        fontSize: normalize(16),
    },
    buttonGroup: {
        flexDirection: 'column',
        width: '100%',
        height: height * 0.15,
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    header: {
        backgroundColor: 'white',
        width: '100%',
        height: Platform.OS === 'ios' ? '10%' : '12%',
        marginTop: 0
    },
    noShadowHeader: {
        ...Platform.select({
            ios: {
                shadowOffset: {
                    width: 0,
                    height: 0,
                },
                shadowColor: 'transparent',
                shadowOpacity: 0,
            },
            android: {
                elevation: 0
            }
        })
    },
})

export default BluModal;

