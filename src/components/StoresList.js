import React from 'react';
import { View, Text, StyleSheet, FlatList, TouchableOpacity, Platform } from 'react-native';
import StoreDetail from './StoreDetail';
import { withNavigation } from 'react-navigation';
import normalize from '../config/services/normalizeFontSize';
import { height, width } from '../config/constants/screenDimensions';
import colors from '../../assets/colors';

const StoresList = ({ title, results, navigation }) => {
    if (!results.length) {
        return null;
    }

    return (
        <View style={styles.container}>
            <Text style={styles.title}>{title}</Text>
            <FlatList
                showsVerticalScrollIndicator={false}
                data={results}
                keyExtractor={(result) => result.id}
                renderItem={({ item }) => {
                    return (
                        <TouchableOpacity
                            onPress={() =>
                                navigation.navigate('ResultsShow', { id: item.id })}
                        >
                            <StoreDetail
                                result={item}
                            />
                        </TouchableOpacity>
                    );
                }}
            />
        </View>
    );
};

const {
    lightShadow,
    darkGray,
    gray,
    blueGray,
    pattensBlue
} = colors;

const styles = StyleSheet.create({
    title: {
        fontSize: Platform.isPad ? normalize(24) : normalize(22),
        fontFamily: 'bold',
        color: lightShadow,
        marginLeft: 15,
        marginBottom: 5
    },
    container: {
        marginBottom: 10,
        backgroundColor: 'white',
        flex:1
    }
});

export default withNavigation(StoresList);