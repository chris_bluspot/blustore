import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    StyleSheet,
    Platform,
} from 'react-native';
import BackButton from './BackButton';
import normalize from '../config/services/normalizeFontSize';
import { height, width } from '../config/constants/screenDimensions';

const Header = ({
    title,
    withoutTitle,
    titleStyle,
    buttonType,
    buttonImageStyle,
    buttonStyle,
    onPress,
    imageForButton,
    hasImage,
    imageForHeader,
    container,
    imgStyle,
    dark,
    noShadow,
    customStyle,
    pinToMap,
    navigation
}) => {
    return (
        withoutTitle ?
            <View style={[styles.withoutTitle, titleStyle]}>
                <BackButton
                    onPress={onPress}
                    type={buttonType}
                    container={buttonStyle}
                    imageStyle={[styles.headerImg, buttonImageStyle]}
                    image={imageForButton}
                    dark={dark}
                />
            </View>
            : <View setOutlineSpotShadowColor={'#86E8FF'} style={[styles.headerSection, container, customStyle]}>
                <View style={styles.backButtonSection}>
                    <BackButton
                        onPress={onPress}
                        type={buttonType}
                        container={buttonStyle}
                        imageStyle={[styles.headerImg, buttonImageStyle]}
                        image={imageForButton}
                        dark={dark}
                    />
                </View>
                {pinToMap &&
                    <View style={{
                        //backgroundColor: '#F4F4F4', 
                        height: '100%', width: '10%', borderRadius: 50,
                        position: 'absolute', zIndex: 1,
                        justifyContent: 'center',
                        top: '3.3%',
                        left: '85%',
                        bottom: '0%',
                        right: '0%'
                    }}>
                        <TouchableOpacity
                            onPress={() => navigation.navigate('mapScreen')}
                        >
                            <Image
                                source={require('../../assets/images/pin-no-hours.png')}
                                style={{ width: 20, height: 35, alignSelf: 'center' }}
                                resizeMode={'contain'}
                            />
                        </TouchableOpacity>
                    </View>
                }
                {hasImage ?
                    <View style={styles.headerTitleSection}>
                        <View style={styles.secondRow}>
                            <Image
                                source={imageForHeader}
                                style={imgStyle}
                            />
                            <Text numberOfLines={1} ellipsizeMode={'tail'} style={[styles.headerTitle, titleStyle]}>{title}</Text>
                        </View>
                    </View>
                    :
                    <View style={styles.headerTitleSection}>
                        <Text numberOfLines={1} ellipsizeMode={'tail'} style={[styles.headerTitle, titleStyle]}>{title}</Text>
                    </View>
                }
            </View>
    )
};

const styles = StyleSheet.create({
    headerSection: {
        flexDirection: 'row',
        width: '100%',
        height: '6%',
        justifyContent: 'space-around',
        //backgroundColor: '#00AFD7',
        backgroundColor: 'white',
        borderBottomLeftRadius: 15,
        borderBottomRightRadius: 15
    },
    backButtonSection: {
        //flex:2,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        alignSelf: 'flex-start',
        width: '20%',
        height: '100%',
        zIndex: 20
    },
    headerTitleSection: {
        flexDirection: 'column',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    headerTitle: {
        //color: 'white',
        color: '#00AFD7',
        fontSize: Platform.isPad ? normalize(28) : (height < 680 ? normalize(25) : normalize(26)),
        fontFamily: 'regular',
    },
    headerImg: {
        resizeMode: 'contain'
    },
    secondRow: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    shadowOffForModal: {
        ...Platform.select({
            ios: {
                shadowOffset: {
                    width: 0,
                    height: height * 0.01
                },
                shadowColor: '#86E8FF',
                shadowOpacity: 0.195,
            },
            android: {
                elevation: 0
            }
        })
    },
    withoutTitle: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
    }
});

export default Header;