import React from 'react';
import { View, Image, Text, StyleSheet, Platform } from 'react-native';
import normalize from '../config/services/normalizeFontSize';
import { height, width } from '../config/constants/screenDimensions';
import colors from '../../assets/colors';

const StoreDetail = ({ result }) => {
  return (
    <View style={styles.container}>
      <Image
        source={{ uri: result.image_url }}
        style={styles.image}
      />
      <Text style={styles.name}>{result.name}</Text>
      <View style={styles.addressContainer}>
        <Text style={styles.addressTitle}>Dirección: </Text>
        <Text style={styles.addressText}>{result.location.address1}, {result.location.city}</Text>
      </View>
    </View>
  );
};

const {
  lightShadow,
  darkGray,
  gray,
  blueGray,
  pattensBlue
} = colors;

const styles = StyleSheet.create({
  container: {
    marginLeft: 15
  },
  addressContainer: {
    flexDirection: 'row'
  },
  addressTitle: {
    fontFamily: 'bold',
    fontSize: Platform.isPad ? normalize(20) : normalize(18),
  },
  addressText: {
    fontFamily: 'light',
    fontSize: Platform.isPad ? normalize(20) : normalize(18),
  },
  image: {
    width: 250,
    height: 120,
    borderRadius: 4,
    marginBottom: 5
  },
  name: {
    fontFamily: 'bold',
    fontSize: Platform.isPad ? normalize(22) : normalize(20),
  }
});

export default StoreDetail;
