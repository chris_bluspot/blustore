import React, { Component, useState } from 'react';
import {
    TouchableOpacity,
    Text,
    View,
    Image,
    StyleSheet,
    Platform,
} from 'react-native';
//import PropTypes from 'prop-types';
import BluActivityIndicator from './BluActivityIndicator';
import colors from '../../assets/colors';
import { height } from '../config/constants/screenDimensions';
import ratio from '../config/constants/ratio';
import normalize from '../config/services/normalizeFontSize';

const BluButton = ({
    title,
    onPress,
    container,
    isDark,
    hasShadow,
    hasActivityIndicator,
    activityIndicatorStatus,
    activityIndicatorStyle,
    disabled,
    textStyle,
    img,
    imgStyle
}) => {
    const shadow = hasShadow ? styles.shadow : null;
    const darkShadow = hasShadow && isDark ? styles.darkShadow : null;
    const background = isDark ? styles.darkBackground : null;
    if (hasActivityIndicator && activityIndicatorStatus) {
        return (
            <BluActivityIndicator color={colors.bluspotColor} style={activityIndicatorStyle} />
        )
    }
    if (disabled) {
        return (
            <View style={[styles.container, container, shadow, darkShadow, { backgroundColor: 'gray' }]}>
                <Text style={[styles.title, textStyle]}>{title}</Text>
            </View>
        )
    }
    return (
        <TouchableOpacity
            style={[
                styles.container,
                container,
                shadow,
                darkShadow,
                background
            ]}
            onPress={onPress}
        >
            {img && <Image
                source={img}
                style={imgStyle}
                resizeMode={'contain'}
            />}
            <Text style={[styles.title, textStyle]}>{title}</Text>
        </TouchableOpacity>
    )
};

const {
    blue,
    darkBlue,
    lightShadow
} = colors;

const styles = StyleSheet.create({
    container: {
        height: height > 800 ? height * 0.055 :
            (height > 600 ? height * 0.065 : height * 0.07),
        width: '80%',
        flexDirection: 'row',
        backgroundColor: blue,
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
    },
    shadow: {
        ...Platform.select({
            ios: {
                shadowOffset: {
                    width: 0,
                    height: height * 0.007
                },
                shadowColor: lightShadow,
                shadowOpacity: 0.195,
            },
            android: {
                elevation: 3,
            }
        })
    },
    title: {
        ...Platform.select({
            ios: {
                fontSize: ratio >= 2 ? normalize(16) : normalize(15),
            },
            andorid: {
                fontSize: normalize(15),
            }
        }),
        color: 'white',
        fontFamily: 'bold'
    },
    darkBackground: {
        backgroundColor: darkBlue,
    },
    darkShadow: {
        shadowColor: darkBlue,
    },
});

export default BluButton;