import React, { useState, useEffect } from 'react';
import { View, StyleSheet } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import HomeScreen from './src/screens/HomeScreen';
import SignUpScreen from './src/screens/SignUpScreen';
import CodeScreen from './src/screens/CodeScreen';
import ResultsShowScreen from './src/screens/ResultsShowScreen';
import firebase from 'firebase';
import firebaseConfig from './src/config/constants/firebaseConfig';
import * as Font from 'expo-font';
import { Asset } from 'expo-asset';
import { fonts } from './src/config/constants/fonts';
import { images } from './src/config/constants/images';
import colors from './assets/colors';
import BluActivityIndicator from './src/components/BluActivityIndicator';
import ratio from './src/config/constants/ratio';

const App = () => {
  const [initialScreen, setInitialScreen] = useState('');
  const [loading, setLoading] = useState(true);

  const _loadResourcesAsync = async () => {
    try {
      let location = await Location.getCurrentPositionAsync({ enableHighAccuracy: true });
      await AsyncStorage.setItem('location', JSON.stringify(location))
      await Font.loadAsync(fonts);
      const cacheImages = images.map(async (image) => {
        return await Asset.fromModule(image).downloadAsync();
      });
      return Promise.all(...cacheImages);
    } catch (err) {
      await Font.loadAsync(fonts);
      const cacheImages = images.map(async (image) => {
        return await Asset.fromModule(image).downloadAsync();
      });
      return Promise.all(...cacheImages);
    }

  }

  useEffect(() => {
    if (!firebase.apps.length) {
      firebase.initializeApp(firebaseConfig);
    }

    _loadResourcesAsync().then(() => {
      firebase.auth().onAuthStateChanged(user => {
        if (user) {
          console.log('user; ', user);
          setInitialScreen('Home');
          setLoading(false);
        } else {
          setInitialScreen('SignUp');
          setLoading(false);
        }
      });
    });

  }, []);

  const navigator = createStackNavigator({
    Home: HomeScreen,
    SignUp: SignUpScreen,
    Code: CodeScreen,
    ResultsShow: ResultsShowScreen
  }, {
    initialRouteName: initialScreen,
    defaultNavigationOptions: {
      headerShown: false
    }
  });

  const NavigatorContainer = createAppContainer(navigator);

  console.log('initialScreen: ', initialScreen);
  if (!loading) {
    return <NavigatorContainer />
  }
  else {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <BluActivityIndicator color={colors.bluspotColor} style={styles.activityIndicator}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  activityIndicator: {
    top: ratio > 2 ? '21%' : '17%',
    position: 'absolute',
    alignSelf: 'center',
    zIndex: 15
  },
});

export default App;